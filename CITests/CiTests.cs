using CiConsole;

namespace CITests
{
    [TestFixture]
    public class CiTests
    {
        [TestCase(1,1,2)]
        [TestCase(2,0,2)]
        [TestCase(3,-5,-2)]
        public void AddTest(double x, double y, double result)
        {
            double output = Calculator.Add(x, y);

            Assert.That(output, Is.EqualTo(result));
        }

        [TestCase(1, 1, 0)]
        [TestCase(2, 0, 2)]
        [TestCase(3, -5, 8)]
        public void SubstractTest(double x, double y, double result)
        {
            double output = Calculator.Substract(x, y);

            Assert.That(output, Is.EqualTo(result));
        }

        [TestCase(1, 1, 1)]
        [TestCase(2, 0, 0)]
        [TestCase(3, -5, -15)]
        public void MultiplyTest(double x, double y, double result)
        {
            double output = Calculator.Multiply(x, y);

            Assert.That(output, Is.EqualTo(result));
        }

        [TestCase(1, 1, 1)]
        [TestCase(3, -2, -1.5)]
        public void DivideTest(double x, double y, double result)
        {
            double output = Calculator.Divide(x, y);

            Assert.That(Math.Abs(output - result), Is.LessThan(10E-6));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void DivideTestThrowsException(double x)
        {
            ArgumentException? ex = Assert.Throws<ArgumentException>(() => Calculator.Divide(x, 0));
            Assert.That(ex.Message, Is.EqualTo("Divider cannot be zero (Parameter 'y')"));
            Assert.That(ex.ParamName, Is.EqualTo("y"));
        }
    }
}